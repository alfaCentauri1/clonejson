package com.alfaCentauri;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.rits.cloning.Cloner;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

public class Ejecutable {

    public static void main(String []args) {
        System.out.println("Ejemplo de clonación de un objeto json.");
        String salida = "";
        Cloner cloner = new Cloner();
        Map myLinkedHashMap = new LinkedHashMap<>();
        Gson gson = new Gson();
        System.out.println("Programa de ejemplo para crear un json con GSon.");
        myLinkedHashMap.put("nombre", "Gato con botas");
        myLinkedHashMap.put("edad", "3");
        String[] arrayComidas = {"Leche", "Pollo", "Atun", "Ensalada", "Sopa de pollo"};
        myLinkedHashMap.put("comidas", arrayComidas);
        Map adress = new HashMap();
        adress.put("calle", "Paraguay");
        adress.put("numero", "610");
        adress.put("piso", "20");
        myLinkedHashMap.put("direccion",adress);
        myLinkedHashMap.put("estadoActivado", true);
        myLinkedHashMap.put("estadoEspera", false);
        myLinkedHashMap.put("cantidad", 15);
        myLinkedHashMap.put("telefono", 54112345678L);
        myLinkedHashMap.put("precio",34.5);
        System.out.println("myLinkedHashMap: " + myLinkedHashMap);
        Object mapa = myLinkedHashMap.get("direccion");
        if( mapa instanceof Map) {
            System.out.println("Es un mapa " + mapa);
        }
        Object arreglo = myLinkedHashMap.get("comidas");
        if( arreglo instanceof String[]) {
            System.out.println("Es un arreglo " + arreglo);
            int max = ((String[]) arreglo).length;
            for (Object element : (String[])arreglo){
                System.out.println("Elemento # " + element);
            }
            System.out.println("Cantidad de elementos del arreglo " + max);
        }
        //Para clonar el objeto se utiliza reflect
        //Gson clon = cloner.deepClone(gson);
        Gson copia1 = new Gson();
        copia1.toJson( gson.toString() );
        gson.fromJson((JsonElement) adress, Map.class);
        System.out.println("El original es: " + gson.toString());
        System.out.println("La copia es: " + copia1.toString());
        //
        //System.out.println( "El clon es:\n" + clon.toString() );
        System.out.println("Fin del ejemplo.");
    }
}
